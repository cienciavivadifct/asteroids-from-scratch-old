from panda3d.core import NodePath
from panda3d.core import LPoint2, LVector2
from panda3d.core import LPoint3, LVector3
from panda3d.core import TransparencyAttrib

class GameActor(NodePath):
    def __init__(self, 
                 tex=None, 
                 pos=LPoint2(0,0), 
                 size=LVector2(32,32),
                 transparency=True):
        obj = loader.loadModel("models/plane")
        NodePath.__init__(self, obj)
        self.reparentTo(camera)

        # Set the initial position and scale of the actor
        self.setPos(pos.getX(), 0.5, pos.getY())
        self.setScale(size.getX(), 1, size.getY())

        self.setDepthTest(False)

        if transparency:
            # Enable transparency blending
            self.setTransparency(TransparencyAttrib.MAlpha)

        if tex:
            # Load and set the requested texture
            img = loader.loadTexture("textures/" + tex)
            self.setTexture(img, 1)

    # sets the current position
    def setPosition(self, pos):
        """Sets the current position of the actor.

        Args:
            pos (LPoint2): Position (x,y) of the top left corner of the actor.
        """
        newPos = LPoint3(pos.getX(), self.getPos().getY(), pos.getY())
        self.setPos(newPos)

    # return the current position
    def getPosition(self):
        pos3d = self.getPos()
        return LPoint2(pos3d.getX(), pos3d.getZ())

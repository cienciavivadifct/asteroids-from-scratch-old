from gameactor import GameActor
from panda3d.core import LVector2

class Ship(GameActor):

    def __init__(self, pos):
        GameActor.__init__(self, "ship.png", pos, LVector2(16,16))

    def rotate(self,ammount):
        heading = self.getR()
        heading += ammount
        self.setR(heading % 360)
        
    def rotateLeft(self,ammount):
        self.rotate(ammount)
    
    def rotateRight(self,ammount):
        self.rotate(-ammount)

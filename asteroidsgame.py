import sys
from game2d import Game2D
from gameactor import GameActor
from panda3d.core import LPoint2, LVector2
from ship import Ship

class AsteroidsGame(Game2D):
    """
    Class for our AsteroidsGame.
    """
    def __init__(self):
        """
        Constructs a new AsteroidsGame.
        Append game specific initialization code at the end.
        """
        # We always need to call the __init__() function from the parent class.
        Game2D.__init__(self)
        # Our specific code goes here...
        self.createActors()
        
        # Initial state of the keys dictionary
        self.keys = {
            "left": 0,
            "right": 0,
            "accel": 0,
            "fire": 0
        }

        self.accept("escape", sys.exit)  # Escape quits
        # Other keys events set the appropriate value in our key dictionary
        self.accept("arrow_left",     self.setKey, ["left", 1])
        self.accept("arrow_left-up",  self.setKey, ["left", 0])
        self.accept("arrow_right",    self.setKey, ["right", 1])
        self.accept("arrow_right-up", self.setKey, ["right", 0])
        self.accept("arrow_up",       self.setKey, ["accel", 1])
        self.accept("arrow_up-up",    self.setKey, ["accel", 0])
        self.accept("space",          self.setKey, ["fire", 1])
        self.accept("space-up",       self.setKey, ["fire", 0])

    def gameLoop(self, task, dt):
        """
        This is our game loop, where all the frame by frame logic will happen.
        We need to return Game2D.cont to keep the game loop running.
        This gets called after the base class Game2D has done all its general game loop activities.

        Parameters
        ----------
            task : Task
                The task that is associated with our game loop
            dt : float
                The elapsed time since the last call to this method
        Returns
        -------
            One of the values {Game2D.cont, Game2D.done, Game2D.exit, ...}
        """
        if self.keys['left']:
            self.ship.rotateLeft(dt)
        elif self.keys['right']:
            self.ship.rotateRight(dt)
        
        return Game2D.cont

    def createActors(self):
        # create the background
        bg = GameActor("stars.jpg",
                       LPoint2(self.width/2,self.height/2),
                       LVector2(self.width,self.height),
                       False)
        bg.setBin("background", 10)

        # create the ship
        self.ship = Ship(LPoint2(self.width/2, self.height/2))
        self.ship.setBin("fixed", 40)

    def setKey(self, key, val):
        self.keys[key] = val

# Let us create an object from our AsteroidsGame class...
game = AsteroidsGame()
# ... and ask it to run
game.run()
